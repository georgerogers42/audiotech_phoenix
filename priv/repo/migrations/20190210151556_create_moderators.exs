defmodule Audiotech.Repo.Migrations.CreateModerators do
  use Ecto.Migration

  def change do
    create table(:moderators) do
      add :user_id, references(:users)
      timestamps()
    end
  end
end
