defmodule Audiotech.Repo.Migrations.CreateArticles do
  use Ecto.Migration

  def change do
    create table(:articles) do
      add :user_id, references(:users)
      add :title, :string
      add :content, :text

      timestamps()
    end
  end
end
