defmodule Audiotech.Repo.Migrations.UniqueUserIdForModerator do
  use Ecto.Migration

  def change do
    create unique_index(:moderators, [:user_id])
  end
end
