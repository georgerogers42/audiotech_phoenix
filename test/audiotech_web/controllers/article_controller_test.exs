defmodule AudiotechWeb.ArticleControllerTest do
  use AudiotechWeb.ConnCase

  test "GET /articles", %{conn: conn} do
    conn = get(conn, "/articles")
    assert html_response(conn, 200) =~ "Home"
  end

  test "POST /article/create", %{conn: conn} do
    conn =
      post(conn, "/signup", %{
        "name" => "George F Rogers",
        "username" => "gfr",
        "password" => "password",
        "email" => "gfr@example.com"
      })

    assert redirected_to(conn, 302) === "/user/gfr"
    conn = post(conn, "/article/create", %{"title" => "Hello World", "content" => "Soda"})
    u = redirected_to(conn, 302)
    assert Regex.match?(~r"^/articles/\d+", u)
    conn = get(conn, u)
    html = html_response(conn, 200)
    assert Regex.match?(~r(Hello World), html)
    assert Regex.match?(~r"<div.*>(.|\n)*Soda(.|\n)*</div>", html)
  end
end
