defmodule AudiotechWeb.PageControllerTest do
  use AudiotechWeb.ConnCase

  test "Sign in", %{conn: conn} do
    conn =
      post(conn, "/signup", %{
        "name" => "George F Rogers",
        "username" => "gfr",
        "password" => "password",
        "email" => "gfr@example.com"
      })

    assert redirected_to(conn, 302) === "/user/gfr"
    assert get_flash(conn, :info) === "Signed Up"
    conn = post(conn, "/logout")
    assert redirected_to(conn, 302) === "/"
    conn = post(conn, "/login", %{"username" => "gfr", "password" => "password"})
    assert redirected_to(conn, 302) === "/user/gfr"
    assert get_flash(conn, :info) === "Logged in as: gfr"
  end

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Username:"
  end
end
