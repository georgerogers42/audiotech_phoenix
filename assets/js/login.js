"use strict";
//
// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.css";

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html";

import $ from "./jquery";
import moment from "./moment"

function doConfirm(msg) {
  return () => {
    return confirm(msg);
  };
}
$(function() {
  $("#logout").on("submit", doConfirm("Logout?"));
  $("#terminate").on("submit", doConfirm("Terminate Account?"));
  $("#delete-article").on("submit", doConfirm("Delete Article?"));
  for(var span of $(".date")) {
    var $span = $(span);
    $span.text(moment($span.text()).format());
  }
});
