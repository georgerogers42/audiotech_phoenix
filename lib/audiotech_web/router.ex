defmodule AudiotechWeb.Router do
  use AudiotechWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", AudiotechWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/user/:username", PageController, :user
    get "/articles/", ArticleController, :index
    get "/articles/:id", ArticleController, :show
    post "/article/create", ArticleController, :create
    post "/articles/:id/edit", ArticleController, :edit
    post "/articles/:id/delete", ArticleController, :delete
    post "/login", PageController, :login
    post "/logout", PageController, :logout
    post "/update", PageController, :update
    post "/signup", PageController, :signup
    post "/terminate", PageController, :terminate
  end

  # Other scopes may use custom stacks.
  # scope "/api", AudiotechWeb do
  #   pipe_through :api
  # end
end
