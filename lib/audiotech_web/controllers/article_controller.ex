defmodule AudiotechWeb.ArticleController do
  use AudiotechWeb, :controller
  import Ecto.Query, only: [from: 2]

  def index(conn, _params) do
    cusername = get_session(conn, :username) || ""
    {cuser, cmod} = Audiotech.Users.load_user(cusername)
    articles = from(a in Audiotech.Article, preload: [:user], order_by: [desc: [a.updated_at]])

    users =
      from(u in Audiotech.Users,
        join: a in Audiotech.Article,
        on: u.id == a.user_id,
        preload: [articles: ^articles],
        order_by: [desc: max(a.inserted_at)],
        group_by: u.id
      )
      |> Audiotech.Repo.all()

    env = %{
      title: "articles",
      token: get_csrf_token(),
      cuser: cuser,
      cmod: cmod,
      users: users
    }

    render(conn, "index.html", env)
  end

  def show(conn, params) do
    id = params["id"]
    cusername = get_session(conn, :username) || ""
    {cuser, cmod} = Audiotech.Users.load_user(cusername)

    case Audiotech.Repo.all(from(a in Audiotech.Article, where: a.id == ^id, preload: [:user])) do
      [article] ->
        env = %{
          title: article.title,
          cuser: cuser,
          cmod: cmod,
          article: article,
          token: get_csrf_token()
        }

        conn
        |> render("show.html", env)

      _ ->
        conn
        |> put_status(404)
        |> put_view(AudiotechWeb.ErrorView)
        |> render("404.html")
    end
  end

  def edit(conn, params) do
    id = params["id"]
    username = get_session(conn, :username)

    {:ok, article} =
      Audiotech.Repo.transaction(fn ->
        user =
          from(u in Audiotech.Users, where: u.username == ^username)
          |> Audiotech.Repo.one!()

        article =
          from(a in Audiotech.Article,
            left_join: m in Audiotech.Moderator,
            where: a.id == ^id and (a.user_id == ^user.id or (m.user_id == ^user.id and not is_nil(m.id)))
          )
          |> Audiotech.Repo.one!()

        Audiotech.Repo.update(
          article
          |> Audiotech.Article.changeset(%{title: params["title"], content: params["content"]})
        )

        article
      end)

    conn |> redirect(to: "/articles/#{article.id}")
  end

  def delete(conn, params) do
    id = params["id"]
    username = get_session(conn, :username)

    {:ok, _} =
      Audiotech.Repo.transaction(fn ->
        user = Audiotech.Repo.one!(from(u in Audiotech.Users, where: u.username == ^username))

        article =
          Audiotech.Repo.one!(
            from(a in Audiotech.Article,
              join: m in Audiotech.Moderator,
              where: a.id == ^id and (a.user_id == ^user.id or m.user_id == ^user.id)
            )
          )

        Audiotech.Repo.delete!(article)
      end)

    conn |> redirect(to: "/user/#{username}")
  end

  def create(conn, params) do
    username = get_session(conn, :username)

    {:ok, article} =
      Audiotech.Repo.transaction(fn ->
        user =
          from(u in Audiotech.Users, where: u.username == ^username)
          |> Audiotech.Repo.one!()

        article = %Audiotech.Article{
          user_id: user.id,
          title: params["title"],
          content: params["content"]
        }

        Audiotech.Repo.insert!(article)
      end)

    conn |> redirect(to: "/articles/#{article.id}")
  end
end
