defmodule AudiotechWeb.PageController do
  import Ecto.Query, only: [from: 2]
  use AudiotechWeb, :controller

  def index(conn, _params) do
    users = Audiotech.Repo.all(Audiotech.Users)
    cusername = get_session(conn, :username) || ""
    {cuser, cmod} = Audiotech.Users.load_user(cusername)
    env = %{
      token: get_csrf_token(),
      users: users,
      cuser: cuser,
      cmod: cmod,
      title: "index"
    }

    render(conn, "index.html", env)
  end

  def user(conn, params) do
    cusername = get_session(conn, :username) || ""
    {cuser, cmod} = Audiotech.Users.load_user(cusername)
    username = params["username"]
    arts = from(a in Audiotech.Article, order_by: [desc: a.updated_at], select: a)

    case from(u in Audiotech.Users, where: u.username == ^username, preload: [articles: ^arts])
         |> Audiotech.Repo.all() do
      [user] ->
        env = %{
          token: get_csrf_token(),
          cuser: cuser,
          cmod: cmod,
          user: user,
          title: user.username
        }

        conn |> render("user.html", env)

      _ ->
        conn
        |> put_status(404)
        |> put_view(AudiotechWeb.ErrorView)
        |> render("404.html")
    end
  end

  def login(conn, params) do
    username = params["username"]
    password = params["password"]
    user = Audiotech.Repo.one(from(u in Audiotech.Users, where: u.username == ^username))

    if user && Pbkdf2.verify_pass(password, user.password) do
      conn
      |> clear_session()
      |> put_session(:username, user.username)
      |> put_flash(:info, "Logged in as: #{user.username}")
      |> redirect(to: "/user/#{user.username}")
    else
      conn |> put_flash(:error, "Bad Login") |> redirect(to: "/")
    end
  end

  def logout(conn, _params) do
    conn
    |> clear_session()
    |> put_flash(:info, "Logged Out")
    |> redirect(to: "/")
  end

  def signup(conn, params) do
    confirm = params["confirm"]
    password = params["password"]

    if password === confirm and String.length(password) <= 512 do
      user = %Audiotech.Users{
        username: params["username"],
        email: params["email"],
        name: params["name"],
        password: Pbkdf2.hash_pwd_salt(params["password"])
      }

      user =
        try do
          {:ok, Audiotech.Repo.insert!(user)}
        rescue
          e in Ecto.ConstraintError ->
            {:error, e}
        end

      case user do
        {:ok, user} ->
          conn
          |> clear_session
          |> put_flash(:info, "Signed up")
          |> put_session(:username, user.username)
          |> redirect(to: "/user/#{user.username}")

        {:error, _} ->
          conn
          |> clear_session
          |> put_flash(:error, "Bad Signup")
          |> redirect(to: "/")
      end
    else
      conn
      |> clear_session
      |> put_flash(:error, "Bad Signup")
      |> redirect(to: "/")
    end
  end

  def update(conn, params) do
    username = params["username"]
    confirm = params["confirm"]
    password = params["password"]
    cusername = get_session(conn, :username)

    if cusername === username && password === confirm do
      user =
        from(u in Audiotech.Users, where: u.username == ^cusername)
        |> Audiotech.Repo.one()
        |> Audiotech.Users.changeset(%{password: Pbkdf2.hash_pwd_salt(password)})
        |> Audiotech.Repo.update!()

      conn
      |> clear_session
      |> put_session(:username, user.username)
      |> put_flash(:info, "Updated Password")
      |> redirect(to: "/user/#{user.username}")
    else
      conn
      |> clear_session
      |> put_flash(:info, "Bad Password Change")
      |> redirect(to: "/")
    end
  end

  def terminate(conn, _params) do
    username = get_session(conn, :username)

    if username do
      {:ok, _} =
        Audiotech.Repo.transaction(fn ->
          user =
            from(u in Audiotech.Users, where: u.username == ^username)
            |> Audiotech.Repo.one!()

          Audiotech.Repo.delete!(user)
        end)

      conn
      |> clear_session()
      |> put_flash(:info, "Deleted Account")
      |> redirect(to: "/")
    else
      conn
      |> clear_session()
      |> redirect(to: "/")
    end
  end
end
