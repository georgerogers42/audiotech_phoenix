defmodule Audiotech.Users do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]

  schema "users" do
    field :email, :string
    field :name, :string
    field :password, :string
    field :username, :string
    has_one :moderator, Audiotech.Moderator, foreign_key: :user_id
    has_many :articles, Audiotech.Article, foreign_key: :user_id, on_delete: :delete_all

    timestamps()
  end

  @doc false
  def changeset(users, attrs) do
    users
    |> cast(attrs, [:username, :email, :name, :password])
    |> validate_required([:username, :email, :name, :password])
  end

  def load_user(username) do
    q = from(u in Audiotech.Users, where: u.username == ^username, preload: [:moderator])

    case Audiotech.Repo.all(q) do
      [cuser] ->
        {cuser, cuser.moderator}
      _ ->
        {nil, nil}
    end
  end
end
