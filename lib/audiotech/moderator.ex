defmodule Audiotech.Moderator do
  use Ecto.Schema
  import Ecto.Changeset

  schema "moderators" do
    belongs_to :user, Audiotech.Users
    timestamps()
  end

  @doc false
  def changeset(moderator, attrs) do
    moderator
    |> cast(attrs, [:user_id])
    |> validate_required([:user_id])
  end
end
