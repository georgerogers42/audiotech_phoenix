defmodule Audiotech.Repo do
  use Ecto.Repo,
    otp_app: :audiotech,
    adapter: Ecto.Adapters.Postgres
end
