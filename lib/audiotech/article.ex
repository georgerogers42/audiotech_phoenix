defmodule Audiotech.Article do
  use Ecto.Schema
  import Ecto.Changeset

  schema "articles" do
    field :content, :string
    field :title, :string
    belongs_to :user, Audiotech.Users

    timestamps()
  end

  def contents(article) do
    opts = %Earmark.Options{sanitize: true}
    {:safe, Earmark.as_html!(article.content, opts)}
  end

  @doc false
  def changeset(article, attrs) do
    article
    |> cast(attrs, [:user_id, :title, :content])
    |> validate_required([:user_id, :title, :content])
  end
end
