defmodule Audiotech.DateTime do
  def from_naive(ndt, z \\ "Z") do
    {:ok, dt, _} = DateTime.from_iso8601(NaiveDateTime.to_iso8601(ndt) <> z)
    dt
  end
end
