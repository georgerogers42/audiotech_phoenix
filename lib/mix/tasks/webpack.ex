defmodule Mix.Tasks.Webpack do
  use Mix.Task

  def run(_args) do
    Mix.Task.run(:"phx.digest.clean")
    System.cmd("npm", ["run", "deploy"], cd: "assets", into: IO.stream(:stdio, :line))
    Mix.Task.run(:"phx.digest")
  end
end
